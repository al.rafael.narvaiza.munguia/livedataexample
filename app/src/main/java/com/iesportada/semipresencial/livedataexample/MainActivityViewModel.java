package com.iesportada.semipresencial.livedataexample;

import android.os.CountDownTimer;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainActivityViewModel extends ViewModel {

    private CountDownTimer timer;
    private MutableLiveData<Integer> _seconds = new MutableLiveData<Integer>();
    private MutableLiveData<Boolean> finished = new MutableLiveData<Boolean>();
    private MutableLiveData<Long> timerValue = new MutableLiveData<Long>();

    public void startTimer(){

        timer = new CountDownTimer(timerValue.getValue(), 1000 ) {
            @Override
            public void onTick(long millisUntilFinished) {
                long timeLeft = millisUntilFinished/1000;

                get_seconds().setValue((int)timeLeft);
            }

            @Override
            public void onFinish() {

                getFinished().setValue(true);
            }
        }.start();
    }

    public void stopTimer(){
        timer.cancel();
        getFinished().setValue(true);
    }



    public MutableLiveData get_seconds() {
        return _seconds;
    }


    public MutableLiveData getFinished() {
        return finished;
    }


    public MutableLiveData getTimerValue() {
        return timerValue;
    }

    public void setTimerValue(long timerValue) {
        this.timerValue.setValue(timerValue);
    }
}
