package com.iesportada.semipresencial.livedataexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.iesportada.semipresencial.livedataexample.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        binding.buttonCancel.setOnClickListener(this);
        binding.buttonStart.setOnClickListener(this);

        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);


        viewModel.get_seconds().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                binding.textViewNumber.setText(String.valueOf(integer));

            }
        });
        viewModel.getFinished().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean b) {
                if(b){
                    showMessage("timer finished.");
                }

            }
        });

    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v == binding.buttonStart){
            if(Integer.valueOf(binding.editTextNumber.getText().toString())<1000){
                showMessage("Input number bigger than 1000");
            }
            else {
                viewModel.setTimerValue(Long.valueOf(binding.editTextNumber.getText().toString()));
                viewModel.startTimer();
                binding.editTextNumber.setVisibility(View.INVISIBLE);
                binding.textViewNumber.setVisibility(View.VISIBLE);

            }
        }
        if (v == binding.buttonCancel){
            viewModel.stopTimer();
            binding.editTextNumber.setText("");
            binding.editTextNumber.setVisibility(View.VISIBLE);
            binding.textViewNumber.setVisibility(View.INVISIBLE);
        }

    }

    public void showMessage(String s){
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }
}